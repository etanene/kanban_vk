import React, {Component} from 'react';

const formStyle = {
	display: 'flex',
	flexDirection: 'column',
};

const nameStyle = {
	width: '100%',
	boxSizing: 'border-box',
	padding: '5px',
	border: 'none',
	// boxShadow: '0 2px 2px -5px',
	borderRadius: '5px',
	fontSize: '16px',

};

const buttonsStyle = {
	display: 'flex',
	justifyContent: 'space-between',
	alignItems: 'center',
	marginTop: '10px',
};

const addStyle = {
	width: '170px',
	fontSize: '16px',
	backgroundColor: 'rgb(0, 142, 204)',
	borderRadius: '5px',
	color: 'white',
	padding: '5px',
	outline: 'none',
	cursor: 'pointer',
};

const delStyle = {
	fontSize: '22px',
	color: 'rgb(0, 142, 204)',
	cursor: 'pointer',
};

class AddForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: ''
		};
	};

	handleSubmit = (event) => {
		event.preventDefault();
		if (this.state.name) {
			this.props.setName(this.state.name, this.props.ind);
		}
	};

	handleChange = (event) => {
		this.setState({
			name: event.target.value
		});
	}

	render() {
		let ph = `Введите название ${this.props.title}и`;
		let val = `Добавить ${this.props.title}у`;

		return (
			<form style={formStyle} onSubmit={this.handleSubmit}>
				<input style={nameStyle} type="text" placeholder={ph} value={this.state.name} onChange={this.handleChange} />
				<div style={buttonsStyle}>
					<input style={addStyle} type="submit" value={val} />
					<div style={delStyle} onClick={this.props.cancel}>&#10006;</div>
				</div>
			</form>
		);
	}
}

export default AddForm;