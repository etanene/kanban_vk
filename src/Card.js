import React, {Component} from 'react';

const cardStyle = {
	backgroundColor: 'white',
	borderRadius: '5px',
	padding: '10px 5px',
	marginBottom: '10px',
	wordWrap: 'break-word',
	fontWeight: 'lighter',
};

class Card extends Component {

	onDragStart = (event) => {
		// this.props.delCard(this.props.columnId, this.props.ind);
		event.target.style.opacity = '0.5';
		// event.dataTransfer.effectAllowed = 'move';
	};

	render() {
		return (
			<div style={cardStyle} draggable onDragStart={this.onDragStart}>
				{this.props.text}
			</div>
		);
	}
}

export default Card;