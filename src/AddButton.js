import React, {Component} from 'react';

const buttonStyle = {
	width: '90%',
	textAlign: 'center',
	margin: '5px auto',
	color: 'rgb(0, 142, 204)',
	textWeight: 'bold',
	cursor: 'pointer'
};

class AddButton extends Component {
	render() {
		return (
			<div style={buttonStyle} onClick={this.props.addColumn}>
				&#10010; Добавить еще одну {this.props.title}
			</div>
		);
	}
}

export default AddButton;