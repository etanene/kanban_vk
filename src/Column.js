import React, {Component} from 'react';

import AddButton from './AddButton';
import AddForm from './AddForm';
import Card from './Card';

const columnStyle = {
	backgroundColor: 'rgb(216, 216, 216)',
	width: '300px',
	margin: '5px',
	padding: '10px',
	borderRadius: '5px',
};

const nameStyle = {
	fontSize: '20px',
	marginBottom: '10px',
	wordWrap: 'break-word',
};

class Column extends Component {
	constructor(props) {
		super(props);
		this.state = {
			form: false
		};
	}

	addCard = (name, ind) => {
		this.props.addCard(name, ind);
		this.setState({
			form: false
		});
	};

	render() {
		let action;
		let name;
		let cards;

		if (this.props.addColumn) {
			action = <AddButton title='колонку' addColumn={this.props.addColumn} />;
		} else if (!this.props.data.name) {
			action = <AddForm
						ind={this.props.ind}
						setName={this.props.setName}
						cancel={() => this.props.delColumn(this.props.ind)}
						title="колонк"
					/>
		} else {
			name = <div style={nameStyle}>{this.props.data.name}</div>
			cards = this.props.data.cards.map((card, ind) => {
					return (<Card
								key={ind}
								text={card}
								ind={ind}
								columnId={this.props.ind}
								delCard={this.props.delCard}
							/>);
				});
			if (this.state.form) {
				action = <AddForm
							ind={this.props.ind}
							setName={this.addCard}
							cancel={() => {this.setState({form: false})}}
							title="карточк"
						/>
			} else {
				action = <AddButton
							title='карточку'
							addColumn={() => {this.setState({form: true})}}
						/>
			}
		}

		return (
			<div style={columnStyle}>
				{name}
				{cards}
				{action}
			</div>
		);
	}
}

export default Column;