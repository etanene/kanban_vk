import React, {Component} from 'react';

import Column from './Column';

const appStyle = {
	width: '95%',
	margin: 'auto',
	display: 'flex',
	flexWrap: 'wrap',
	alignItems: 'flex-start'
}

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			columns: [
				{
					name: 'План на месяц',
					cards: [
						'Пройти курс по React',
						'Собрать портфолио',
						'Написать первую статья в блог'
					]
				},
				{
					name: 'Итоги',
					cards: [
						'Сделать колонку итоги',
						'Придумать что-нибудь еще'
					]
				}
			],
		};
	}

	addColumn = () => {
		let arr = this.state.columns;

		arr.push({
			name: '',
			cards: []
		});
		this.setState({
			columns: arr
		});
	};

	delColumn = (ind) => {
		let arr = this.state.columns;

		arr.splice(ind, 1);
		this.setState({
			columns: arr
		});
	};

	setName = (name, ind) => {
		let arr = this.state.columns;

		arr[ind].name = name;
		this.setState({
			columns: arr
		});
	};

	addCard = (text, ind) => {
		let arr = this.state.columns;

		arr[ind].cards.push(text);
		this.setState({
			columns: arr
		});
	};

	delCard = (columnId, ind) => {
		let arr = this.state.columns;

		arr[columnId].cards.splice(ind, 1);
		this.setState({
			columns: arr
		});
	};

	render() {
		return (
			<div style={appStyle}>
				{this.state.columns.map((column, ind) => {
					console.log(this.state.columns);
					return (<Column
								delColumn={this.delColumn}
								setName={this.setName}
								addCard={this.addCard}
								delCard={this.delCard}
								data={column}
								key={ind}
								ind={ind}
							/>);
				})}
				<Column addColumn={this.addColumn} />
			</div>
		);
	};
}

export default App;